# react-minimal

This repo provides an example minimal React project using [parcel](https://en.parceljs.org/).

## Usage

Building
    
    npm run build

Starting the server

    npm run start

### More information

More information is available on [twdev.blog](https://twdev.blog/2024/01/react_minimal/).
